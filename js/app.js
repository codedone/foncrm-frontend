(function (document) {
	//main app object
	var app = document.querySelector('#app');
	//ENUMERATORS-------------------------------------------------------------------------------------------------------
	app.appDomain = '/api';
	app.urls = {
		counterparties: {
			list: app.appDomain + '/counterparties',
			get : app.appDomain + '/counterparties',
			put : app.appDomain + '/counterparties'
		},
		users         : {
			list      : app.appDomain + '/users',
			get       : app.appDomain + '/users',
			put       : app.appDomain + '/users',
			activate  : app.appDomain + '/user/%id/activate',
			deactivate: app.appDomain + '/user/%id/deactivate',
		},
		realties      : {
			list : app.appDomain + '/realties',
			get  : app.appDomain + '/realties',
			put  : app.appDomain + '/realties',
		},
		orders        : {
			list    : app.appDomain + '/orders',
			get     : app.appDomain + '/orders',
			put     : app.appDomain + '/orders',
			complete: app.appDomain + '/orders/%id/complete',
			discard : app.appDomain + '/orders/%id/discard',
			reopen  : app.appDomain + '/orders/%id/reopen',
			suggests: {
				list : app.appDomain + '/orders/%id/realties',
				state: app.appDomain + '/orders/%id/realties/%rid'
			}
		},
		login         : app.appDomain + '/login',
		logout        : app.appDomain + '/logout',
		images        : {
			uploadRealty: app.appDomain + '/realties/images',
			uploadRoom: app.appDomain + '/rooms/images'
		},
		rooms         : {
			list: app.appDomain + '/rooms',
			get: app.appDomain + '/rooms',
			put  : app.appDomain + '/rooms',
			delete  : app.appDomain + '/rooms',
		}
	};
	//order states
	app.orderStates = [
		{
			name : 'open',
			title: 'Открыто'
		},
		{
			name : 'closed',
			title: 'Успешно'
		},
		{
			name : 'rejected',
			title: 'Неудачно'
		}
	];
	//order suggests states
	app.orderSuggestStates = [
		{
			name     : 'unprocessed',
			title    : 'Не обработано',
			color    : 'rgba(255, 255, 255, 1)',
			fontColor: '#03A9F4',
			altColot : '#03A9F4'
		},
		{
			name     : 'compatible',
			title    : 'Подходит',
			color    : 'rgba(76, 175, 80, 1)',
			fontColor: 'rgba(76, 175, 80, 1)',
		},
		{
			name     : 'incompatible',
			title    : 'Не подходит',
			color    : 'rgba(219, 68, 55, 1)',
			fontColor: 'rgba(219, 68, 55, 1)',
		},
		{
			name     : 'pending',
			title    : 'Не дозвонились',
			color    : 'rgba(200, 200, 200, 1)',
			fontColor: '#808080',
		},
		{
			name     : 'processing',
			title    : 'Уточняется',
			color    : 'rgba(255, 152, 0, 1)',
			fontColor: 'rgba(255, 152, 0, 1)',
		}
	];
	app.getOrderSuggestColor = function (name, type) {
		var found = '#000000';
		app.orderSuggestStates.forEach(function (state, state_i, state_arr) {
			if (state.name == name) {
				if (type == 'font') found = state.fontColor;
				else found = state.color;
			}
		});
		return found;
	};
	//realty purposes
	app.realtyPurposes = [
		{
			id   : 1,
			title: 'Офис'
		},
		{
			id   : 2,
			title: 'Склад/Производство'
		},
		{
			id   : 3,
			title: 'Торговое помещение'
		},
		{
			id   : 4,
			title: 'ПСН'
		},
		{
			id   : 5,
			title: 'Земельные участки/Открытые площадки'
		},
		{
			id   : 6,
			title: 'Отдельно стоящее здание'
		}
	];
	//realty offer types
	app.offerType = [
		{
			id   : 1,
			title: 'Аренда',
			name : 'rent'
		},
		{
			id   : 2,
			title: 'Продажа',
			name : 'sale'
		}
	];
	//cooconfigs
	app.cooconfigs = {
		init   : function () {
			app.cooconfigs.configs = $.cookie();
		},
		configs: {},
		set    : function (option, value) {
			$.cookie(option, value, {path: '/'});
		},
		get    : function (option) {
			return $.cookie(option);
		}
	};
	//отопления
	app.heatTypes = [
		{
			name : 'heated',
			title: 'Отапливаемое'
		},
		{
			name : 'warmed',
			title: 'Утепленное'
		},
		{
			name : 'cold',
			title: 'Холодное'
		},
		{
			name : 'open',
			title: 'Открытая площадка,'
		},
		{
			name : 'refrigerator',
			title: 'Холодильник'
		}
	];
	//routesStack
	app.route = {
		prefix: '',
		path  : ''
	};
	app.subroute = {
		prefix: '',
		path  : ''
	};
	app.routeData = {
		section: null
	};
	app.subrouteData = {
		item: null
	};
	app.locationChanged = function(e, o){
	};
	app.routeTail = null;
	app.routing = [
		{
			type: 'section',
			name: 'counterparties',
			id  : 0
		}
	];
	app.routeBack = function () {
		app.routing.pop();
		var last = app.routing[app.routing.length - 1];
		if(typeof last === 'undefined') return;
		switch (last.type) {
			case 'section':
				app.setCurrentSectionByName(last.name, 1)
				break;
			case 'form':
				app.openForm(last.name, last.isNew, last.id, last.requester, 1);
				break;
		}
	};
	//current user information
	app.user = {
		name: '',
		role: 'user'
	};
	app.isAdmin = false;
	//current page
	app.section = {
		current: '',
		last   : ''
	};
	//menu clicked event
	app.menuClicked = function (e) {
		if (e.detail.item.tagName == 'PAPER-SUBMENU') {
			var menu = $(e.detail.item).find('paper-menu')[0];
			menu.set('selected', -1);
			setTimeout(function () {
				$(menu).find('paper-item').each(function () {
					$(this).focusout()
				});
			}, 300);
			return;
		}
		app.$.drawer.closeDrawer();
		var targetMenuItem = e.detail.item;
		var sectionRoute = $(targetMenuItem).attr('route');
		var bundle = $(targetMenuItem).attr('bundle');

		switch (sectionRoute) {
			case 'logout':
				app.logout();
				break;
			default :
				app.setCurrentSectionByName(sectionRoute, null, bundle);
				break;
		}
	};
	//set section by name
	app.setCurrentSectionByName = function (sectionRoute, isFormReopened, bundle) {
		//app.activityTimer.updateTimer();
		if (this.section.current.indexOf('-form') != -1) {
			var poly = $('.page-section#' + sectionRoute)[0];
			poly.set('entryAnimation', 'slide-from-left-animation');
			poly.set('exitAnimation', 'slide-right-animation');

			var latepoly = $('.page-section#' + this.section.current)[0];
			latepoly.set('entryAnimation', 'slide-from-right-animation');
			latepoly.set('exitAnimation', 'slide-right-animation');
		} else {
			var poly = $('.page-section#' + sectionRoute)[0];
			poly.set('entryAnimation', 'slide-from-left-animation');
			poly.set('exitAnimation', 'slide-right-animation');
		}
		app.set('section.last', app.section.current);
		//highlight menu items
		$('.main-menu paper-icon-item').each(function () {
			$(this).removeClass('active');
			if ($(this).attr('route') == sectionRoute) $(this).addClass('active');
		});
		this.$.pages.set('selected', sectionRoute);
		app.set('section.current', sectionRoute);
		app.$.drawer.responsiveWidth = '600px';

		var poly = $('.page-section#' + sectionRoute)[0];
		if (bundle) poly.set('bundle', bundle);
		poly.deselect();

		//set route stack
		if (isFormReopened != 1) {
			app.routing.push({
				type: 'section',
				name: sectionRoute,
				id  : 0
			});
		}

		//routing
		var section = sectionRoute;
		if(poly.bundle) section += '-' + poly.bundle;
		app.set('subrouteData.item', null);
		app.set('routeData.section', section + '/');
	};
	//open object form section
	app.openForm = function (polymerName, isNew, id, requester, isFormReopened) {
		//app.activityTimer.updateTimer();
		if (this.section.current.indexOf('-form') != -1) {
			var poly = $('.page-section#' + polymerName)[0];
			poly.set('entryAnimation', 'slide-from-right-animation');
			poly.set('exitAnimation', 'slide-left-animation');

			var lastPoly = $('.page-section#' + this.section.current)[0];
			lastPoly.set('entryAnimation', 'slide-from-right-animation');
			lastPoly.set('exitAnimation', 'slide-left-animation');
		} else {
			var poly = $('.page-section#' + polymerName)[0];
			poly.set('entryAnimation', 'slide-from-right-animation');
			poly.set('exitAnimation', 'slide-right-animation');

			var lastpoly = $('.page-section#' + this.section.current)[0];
			lastpoly.set('entryAnimation', 'slide-from-left-animation');
			lastpoly.set('exitAnimation', 'slide-left-animation');
		}
		app.set('section.last', app.section.current);
		$('.main-menu paper-icon-item').each(function () {
			$(this).removeClass('active');
			if ($(this).attr('route') == polymerName) $(this).addClass('active');
		});
		var section = $('.page-section#' + polymerName)[0];
		this.$.pages.set('selected', polymerName);
		section.set('isNew', isNew);
		section.set('itemFields', {});
		if (requester) section.set('requester', requester);
		else section.set('requester', {});
		section.open(id, isFormReopened);
		app.set('section.current', polymerName);
		app.$.drawer.responsiveWidth = '1024px';
		//set route stack
		if (isFormReopened != 1) {
			app.routing.push({
				type     : 'form',
				name     : polymerName,
				isNew    : isNew,
				id       : id,
				requester: requester
			});
		} else {
			app.routing.pop();
			app.routing.push({
				type     : 'form',
				name     : polymerName,
				isNew    : isNew,
				id       : id,
				requester: requester
			})
		}

		//routing
		app.set('routeData.section', section.entityRoute + '/');
		app.set('subrouteData.item', id);
	};
	//open modal form
	app.openModal = function (name, entityId, fields, callback) {
		var form = document.getElementById(name);
		form.open(entityId, fields, function (data, object) {
			callback(data, object);
		});
	};
	//app logout
	app.logout = function () {
		$.ajax({
			url     : app.urls.logout,
			method  : 'GET',
			dataType: "json",
			complete: function (e, d) {
				if (!app.checkErrorResponse(e)) return;

				switch (e.status) {
					case 401:
						window.location.href = '/login.html';
						break;
					default :
						app.checkErrorResponse(e);
						break;
				}
			}
		})
		window.location.href = '/login.html';
	};
	//request current user
	app.getUserInfo = function () {
		app.loader.show('get-login');
		var self = this;
		$.ajax({
			url     : app.urls.login,
			method  : 'GET',
			dataType: "json",
			complete: function (e, d) {
				app.loader.hide('get-login');
				if (!app.checkErrorResponse(e)) return;
				switch (e.status) {
					case 200:
						if (typeof app.$ == 'undefined' ||
								typeof app.$.orders == 'undefined' ||
								typeof app.$.realties == 'undefined' ||
								typeof app.$.users == 'undefined' ||
								typeof app.$.counterparties == 'undefined'
						) {
							app.getUserInfo();
							return;
							break;
						}
						app.set('user', e.responseJSON);
						app.set('isAdmin', (e.responseJSON.role == 'admin') ? true : false);
						app.$.orders.setSource();
						app.$.realties.setSource();
						switch (app.user.role) {
							case 'user':
								app.setCurrentSectionByName('orders', 0);
								break;
							case 'admin':
								app.setCurrentSectionByName('counterparties', 0);
								app.$.counterparties.setSource();
								app.$.users.setSource();
								break;
							default:
								app.setCurrentSectionByName('orders', 0);
								break;
						}
						break;
				}
			}
		})
	};
	//page loader
	app.loader = {
		loaders: [],
		show   : function (requester) {
			if (requester) app.loader.loaders.push(requester.toLowerCase());
			$('#loader').css('display', 'block');
			$('#loader').fadeTo(0.5, 0.5);
		},
		hide   : function (requester) {
			if (requester) {
				var index = app.loader.loaders.indexOf(requester.toLowerCase());
				if (index !== -1) {
					app.loader.loaders.splice(index, 1);
				}
			}
			if (!app.loader.loaders.length)
				$('#loader').fadeTo(0.5, 0, function () {
					$('#loader').css('display', 'none');
					loader.isShow = false;
				});
		}
	};
	//page toasts
	app.toaster = {
		show : function (text) {
			$('#toaster').css('background-color', '#323232');
			app.$.toaster.set('text', text);
			app.$.toaster.open();
		},
		error: function (text) {
			$('#toaster').css('background-color', '#e57373');
			app.$.toaster.set('text', text);
			app.$.toaster.open();
		}
	};
	//adding new entity
	app.addItem = function (type, data) {
		//get section
		var section = $('.page-section#' + type)[0];
		section.$$('#grid').refreshItems();
	};
	//update entity
	app.editItem = function (type, data) {
		var section = $('.page-section#' + type)[0];
		section.$$('#grid').refreshItems();
	};
	app.neonSelected = function (e, a) {
		if (!$(a.item).hasClass('page-section')) return;
	};
	app.checkErrorResponse = function (response) {
		if (!response) return response;
		switch (response.status) {
			case 401: {
				app.toaster.show('401: Необходима авторизация');
				window.location.href = '/login.html';
				return 0;
				break;
			}
			case 500: {
				app.toaster.show('500: Неизвестная ошибка сервера');
				return 0;
				break;
			}
			case 404: {
				app.toaster.show('404: Страница не найдена');
				return 0;
				break;
			}
			case 502: {
				app.toaster.show('502: Сервер недоступен');
				return 0;
				break;
			}
			case 504: {
				app.toaster.show('504: Истекло время ожидания');
				return 0;
				break;
			}
			default:
				return response;
				break;
		}
	};
	//get all entity comment
	app.commentsAll = function (entity, id, callback) {
		app.loader.show();
		$.ajax({
			url        : '/api/' + entity + '/' + id + '/comments',
			method     : 'GET',
			contentType: 'application/json',
			complete   : function (e) {
				app.loader.hide();
				if (!app.checkErrorResponse(e)) {
					callback(false);
					return;
				}
				switch (e.status) {
					case 200:
						callback(e.responseJSON);
						break;
				}
			}
		})
	};
	//add new entity comment
	app.addComment = function (entity, id, object, callback) {
		app.loader.show();
		$.ajax({
			url        : '/api/' + entity + '/' + id + '/comments',
			method     : 'PUT',
			contentType: 'application/json',
			data       : JSON.stringify(object),
			complete   : function (e) {
				app.loader.hide();
				if (!app.checkErrorResponse(e)) {
					callback(false);
					return;
				}
				switch (e.status) {
					case 200:
						callback(e.responseJSON);
						break;
				}
			}
		})
	};
	//getting needed handlers parent
	app.getNeededSrc = function (e, selector, type) {
		switch (type) {
			case 'class': {
				if ($(e.srcElement).hasClass(selector)) {
					return e.srcElement;
				} else {
					for (var i = 0; i < e.path.length; i++) {
						if ($(e.path[i]).hasClass(selector)) {
							return e.path[i];
							break;
						}
					}
				}
				break;
			}
			case 'tag': {
				if ($(e.srcElement).prop("tagName").toLowerCase() == selector.toLowerCase()) {
					return e.srcElement;
				} else {
					for (var i = 0; i < e.path.length; i++) {
						if ($(e.path[i]).prop("tagName").toLowerCase() == selector.toLowerCase()) {
							return e.path[i];
							break;
						}
					}
				}
				break;
			}
		}
	};
	//tooltips
	app.showTooltip = function (e, forElem, text) {
		var e = window.event;
		$('#appTooltip #tooltip').html(text);
		$('#appTooltip').attr('for', forElem);
		$('#appTooltip').css('left', e.clientX);
		$('#appTooltip').css('top', e.clientY);
		$('#appTooltip #tooltip').removeClass('hidden');
	};
	app.hideTooltip = function () {
		$('#appTooltip #tooltip').addClass('hidden');
	};
	//images gallery
	app.imgViewer = {
		open          : function (collection, index) {
			app.$.viewer.set('items', collection);
			app.$.viewer.set('selectedIndex', index);
			app.$.viewer.open();
		},
		openFromVaadin: function (event, object, data, section) {
			event.stopPropagation();
			var photos = JSON.parse(decodeURI(data));
			var collection = [];
			photos.forEach(function (img, img_i, img_ar) {
				collection.push({
					type: 'image',
					src : img.path
				})
			});
			app.imgViewer.open(collection, 0);
		}
	};
	//user activity timer
	app.activityTimer = {
		activityTimer: {},
		activityTime : 1000 * 60 * 30,
		//activityTime: 1000*15,
		startTimer   : function () {
			app.activityTimer.activityTimer = setTimeout(app.activityTimer.timerOut, app.activityTimer.activityTime);
			$(document).ajaxComplete(function (e) {
				app.activityTimer.updateTimer();
			});
		},
		updateTimer  : function () {
			clearTimeout(app.activityTimer.activityTimer);
			app.activityTimer.activityTimer = setTimeout(app.activityTimer.timerOut, app.activityTimer.activityTime);
		},
		timerOut     : function () {
			app.logoutMessage();
		}
	};
	//user activity out message
	app.logoutMessage = function () {
		app.showMessage('Время сессии истекло', 'Необходима повторная авторизация', function () {
			app.logout();
		});
	};
	app.showMessage = function (title, content, callback) {
		app.$.messageBox.open(title, content, function (e) {
			callback(e);
		});
	};
	//sections progresses
	app.addLoading = function(sectionName){
		app.$.sectionsProgress.addLoading(sectionName);
	};
	app.removeLoading = function(sectionName){
		app.$.sectionsProgress.removeLoading(sectionName);
	};
})(document);

$("body").on("click", ".drawer-toggle", function (event) {
	app.$.drawer.openDrawer();
});
$(document).on('click', function(){
	app.activityTimer.updateTimer();
});
app.getUserInfo();
app.cooconfigs.init();
app.activityTimer.startTimer();

window.addEventListener('popstate', function(event) {
	app.routeBack();
});