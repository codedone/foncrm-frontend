module.exports = function (grunt) {
	grunt.initConfig({
										 pkg      : grunt.file.readJSON('package.json'),
										 vulcanize: {
											 default: {
												 options: {
													 // Task-specific options go here.
												 },
												 files  : {
													 'elements/components.html': 'elements/elements.html'
												 }
											 }
										 }
									 });

	grunt.loadNpmTasks('grunt-vulcanize');
	grunt.registerTask('default', [
		'vulcanize'
	]);
};